from dataclasses import dataclass
from random import choice as random_choise

import allure
import pytest
from assertpy import assert_that

from framework.brewery_api import (
    get_list_breweries,
    get_random_brewery,
    get_single_brewery,
    search_breweries
)
from test_data.breweries import (
    BREWERIES_NUMBER,
    NOT_IN_THE_LIST_BREWERY_ID,
    SINGLE_BREWERY_ID,
    SINGLE_BREWERY_RESPONSE,
    TOTAL_BREWERIES_NUMBER
)
from test_data.schemas import Brewery, BreweryType
from test_data.states import STATES


@dataclass
class PostalCode:
    """Класс представляет структуру "индекс" для использования в тестовой функции"""
    state: str
    postal_code: str


@pytest.fixture(params=list(STATES.keys()))
def postal_code(request):
    """Фикстура возвращает случайный индекс для каждого из штатов в тестовых данных"""
    return PostalCode(
        state=request.param,
        postal_code=random_choise(STATES[request.param])
    )


def brewery_types_list():
    brewery_types = list(BreweryType)
    brewery_types.remove('proprietor')
    brewery_types.append(pytest.param('proprietor', marks=pytest.mark.xfail(reason=(
        'Метод не возвращает записи по типу proprietor: требует proprieter. '
        'Однако при получении всех записей из БД выясняется, что там лежат '
        'всё же записи с типом proprietor'))))
    return brewery_types


@allure.feature('Проверки функции Single Brewery')
class TestSingleBrewery:
    @allure.title('Проверка получения существующей записи')
    def test_get_single_brewery(self):
        """Проверка получения существующей записи"""
        with allure.step('Отправить запрос с корректным идентификатором'):
            response = get_single_brewery(SINGLE_BREWERY_ID)
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить соответствие ответа схеме пивоварни и полей ожидаемым значениям'):
            response = response.json()
            Brewery.validate(response)
            assert_that(response).is_equal_to(SINGLE_BREWERY_RESPONSE)

    @allure.title('Попытка получения несуществующей записи')
    @pytest.mark.parametrize('brewery_id', ['*', NOT_IN_THE_LIST_BREWERY_ID])
    def test_brewery_not_in_the_list(self, brewery_id):
        """
        Проверка негативного сценария попытки получения несуществующей записи.
        (Requests не дал передать в качестве идентификатора % (кодирует в urlencoded), что вызывает ответ 524)
        Параметризация через параметр.
        """
        with allure.step('Отправить запрос с некорректным идентификатором'):
            response = get_single_brewery(brewery_id)
            assert_that(response.status_code).is_equal_to(404)

        with allure.step('Проверить ответ на сообщение об ошибке'):
            response = response.json()
            assert_that(response["message"]).is_equal_to("Couldn't find Brewery")


@allure.feature('Проверки функции List Breweries')
class TestListBreweries:
    @allure.title('Проверка фильтра per_page')
    def test_per_page(self):
        """Проверка фильтра per_page"""
        with allure.step('Отправить запрос с указанием фильтра per_page'):
            response = get_list_breweries(per_page=BREWERIES_NUMBER)
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ на наличие запрошенного числа пивоварен, а также их соответствие схеме'):
            response = response.json()
            assert_that(response).is_type_of(list).is_length(BREWERIES_NUMBER)
            for brewery in response:
                Brewery.validate(brewery)

    @allure.title('Проверка фильтра brewery_type')
    @pytest.mark.parametrize('brewery_type', brewery_types_list())
    def test_by_type(self, brewery_type):
        """
        Проверка фильтра brewery_type.
        Параметризация через параметр.
        """
        with allure.step('Отправить запрос с указанием фильтра brewery_type'):
            response = get_list_breweries(by_type=brewery_type, per_page=BREWERIES_NUMBER)
            assert_that(
                response.status_code,
                f'Запрос с brewery_type={brewery_type} вернул код {response.status_code}'
            ).is_equal_to(200)

        with allure.step('Проверить ответ на наличие хотя бы одной пивоварни с запрошенным типом'):
            response = response.json()
            assert_that(response).is_type_of(list)
            assert_that(len(response)).is_greater_than_or_equal_to(1)
            for brewery in response:
                assert_that(brewery['brewery_type']).is_equal_to(brewery_type)

    @allure.title('Проверка фильтра by_postal')
    def test_by_postal(self, postal_code):
        """
        Проверка фильтра by_postal.
        Параметризация через фикстуру.
        """
        with allure.step('Отправить запрос с указанием фильтра by_postal'):
            response = get_list_breweries(by_postal=postal_code.postal_code, per_page=BREWERIES_NUMBER)
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ на наличие хотя бы одной пивоварни с запрошенным индексом, '
                         'а также соответствие индексу указанного штата'):
            response = response.json()
            assert_that(response).is_type_of(list)
            assert_that(len(response)).is_greater_than_or_equal_to(1)
            for brewery in response:
                assert_that(brewery['state']).is_equal_to(postal_code.state)


@allure.feature('Проверки функции Random Brewery')
class TestRandomBrewery:
    @allure.title('Проверка получения случайной пивоварни')
    def test_get_random_brewery(self):
        """Проверка получения случайной пивоварни"""
        with allure.step('Отправить запрос на получение случайной пивоварни'):
            response = get_random_brewery()
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ на наличие одной пивоварни, а также соответствие её схеме'):
            response = response.json()
            assert_that(response).is_type_of(list).is_length(1)  # Random Brewery возвращает список с одним элементом
            Brewery.validate(response[0])


@allure.feature('Проверки функции Search Breweries')
class TestSearchBreweries:
    @allure.title('Проверка поиска по различным запросам')
    @pytest.mark.parametrize('query', ['brotherwell', 'texas', 'dickinson', 'company_-_lee', 'company - lee'])
    def test_search_breweries(self, query):
        """
        Проверка поиска по различным запросам.
        Параметризация через параметр.
        """
        with allure.step('Отправить запрос на поиск'):
            response = search_breweries(query=query)
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ на наличие хотя бы одного результата'):
            response = response.json()
            assert_that(response).is_type_of(list)
            assert_that(len(response)).is_greater_than_or_equal_to(1)

        with allure.step('Проверить ответ на наличие поискового запроса в одном из параметров пивоварни'):
            for brewery in response:
                Brewery.validate(brewery)

                query = query.replace('_', ' ')
                query_found = any((
                    query in brewery['id'].replace('-', ' '),
                    query in brewery['name'].lower(),
                    query in brewery['city'].lower(),
                    query in brewery['state'].lower()
                ))
                assert_that(query_found).is_true()

    @allure.title('Проверка пустого запроса')
    def test_search_nothing(self):
        """Проверка пустого запроса"""
        with allure.step('Отправить запрос с пустым поисковым запросом'):
            response = search_breweries(query='')
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ: пустой список'):
            response = response.json()
            assert_that(response).is_type_of(list).is_length(0)

    @allure.title('Проверка запроса на получение всего списка пивоварен')
    def test_search_everything(self):
        """Проверка запроса на получение всего списка пивоварен"""
        with allure.step('Отправить запрос на получение всего списка пивоварен'):
            response = search_breweries(query='*')
            assert_that(response.status_code).is_equal_to(200)

        with allure.step('Проверить ответ на наличие всех доступных пивоварен'):
            response = response.json()
            assert_that(response).is_type_of(list).is_length(TOTAL_BREWERIES_NUMBER)
