import requests

BASE_URL = 'https://api.openbrewerydb.org'


def get_single_brewery(obdb_id):
    url = f'{BASE_URL}/breweries/{obdb_id}'
    response = requests.get(url=url)
    return response


def get_random_brewery():
    url = f'{BASE_URL}/breweries/random'
    response = requests.get(url=url)
    return response


def get_list_breweries(**params):
    url = f'{BASE_URL}/breweries'
    response = requests.get(url=url, params=params)
    return response


def search_breweries(query):
    url = f'{BASE_URL}/breweries/search'
    response = requests.get(url=url, params={'query': query})
    return response
