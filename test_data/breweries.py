SINGLE_BREWERY_ID = 'lemons-mill-brewery-harrodsburg'
NOT_IN_THE_LIST_BREWERY_ID = 'not-in-the-list'
SINGLE_BREWERY_RESPONSE = {
  'id': 'lemons-mill-brewery-harrodsburg',
  'name': 'Lemons Mill Brewery',
  'brewery_type': 'micro',
  'street': '166 Marimon Ave',
  'address_2': None,
  'address_3': None,
  'city': 'Harrodsburg',
  'state': 'Kentucky',
  'county_province': None,
  'postal_code': '40330-1311',
  'country': 'United States',
  'longitude': '-84.83862624',
  'latitude': '37.7608082',
  'phone': '8592650872',
  'website_url': 'http://www.lemonsmillbrewery.com',
  'updated_at': '2021-10-23T02:24:55.243Z',
  'created_at': '2021-10-23T02:24:55.243Z'
}
BREWERIES_NUMBER = 3
TOTAL_BREWERIES_NUMBER = 8131