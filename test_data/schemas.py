from datetime import datetime
from enum import Enum
from re import match
from typing import Union

from pydantic import BaseModel, Field, HttpUrl, validator


class BreweryType(str, Enum):
    micro = 'micro'
    nano = 'nano'
    regional = 'regional'
    brewpub = 'brewpub'
    large = 'large'
    planning = 'planning'
    bar = 'bar'
    contract = 'contract'
    proprietor = 'proprietor'
    closed = 'closed'


class Brewery(BaseModel):
    id: str
    name: str
    brewery_type: BreweryType
    street: Union[str, None] = Field(...)
    address_2: Union[str, None] = Field(...)
    address_3: Union[str, None] = Field(...)
    city: str
    state: str
    county_province: Union[str, None] = Field(...)
    postal_code: str
    country: str
    longitude: Union[float, None] = Field(...)
    latitude: Union[float, None] = Field(...)
    phone: Union[str, None] = Field(...)
    website_url: Union[HttpUrl, None] = Field(...)
    updated_at: datetime
    created_at: datetime

    @validator('state', 'country')
    def check_starts_with_capital(cls, v):
        if not v[0].istitle():
            raise ValueError(f'Значение не начинается с прописной буквы: "{v}"')
        return v

    @validator('postal_code')
    def check_postal_code(cls, v):
        if not match(r'\d{5}(-\d{4})?', v):
            raise ValueError(f'Значение не является допустимым индексом: "{v}"')
        return v
